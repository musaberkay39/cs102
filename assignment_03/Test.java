package cs102.week04;

import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Library csDepartmentLibrary = new Library("CS Department Library", 1234);
        Library ozuClassicsLibrary = new Library("OzU Classics Library", 3423);

        /* Create three Book objects for CS Department Library. */
        Book umlDistilled = new Book("UML Distilled", 175, "0-321-19368-7");
        Book theCProgrammingLanguage = new Book("The C Programming Language", 272, "0-13-110362-8");
        Book designPatterns = new Book("Design Patterns", 395, "0-201-63361-0");

        /* Associate CS Books with CS Department Library. */
        csDepartmentLibrary.addBook(umlDistilled);
        csDepartmentLibrary.addBook(theCProgrammingLanguage);
        csDepartmentLibrary.addBook(designPatterns);

        /* Create three Book objects for OzU Classics Library. */
        Book donQuixote = new Book("Don Quixote", 863, "0-393-09018-3");
        Book theBrothersKaramazov = new Book("The Karamazov Brothers", 824, "0-159-308045-7");
        Book greatExpectations = new Book("Great Expectations", 544, "0-141-43956-3");
        Book mobyDick = new Book("Moby Dick", 536, "0-14-062062-1");

        /* Associate classics with OzU Classics Library. */
        ozuClassicsLibrary.addBook(donQuixote);
        ozuClassicsLibrary.addBook(theBrothersKaramazov);
        ozuClassicsLibrary.addBook(greatExpectations);
        ozuClassicsLibrary.addBook(mobyDick);

        /* Print libraries. */
        System.out.println(csDepartmentLibrary);
        System.out.println(ozuClassicsLibrary);

        /* Interact with the user. */
        Scanner input = new Scanner(System.in);
        while (true) {
            System.out.println("Enter a Book Title to search (Enter an empty string to quit): ");
            String bookTitle = input.nextLine();
            if (bookTitle.equals("")) return;

            if (csDepartmentLibrary.contains(bookTitle)) {
                System.out.println("> CS Department Library has \"" + bookTitle + "\".\n");
            } else if (ozuClassicsLibrary.contains(bookTitle)) {
                System.out.println("> OzU Classics Library has \"" + bookTitle + "\".\n");
            } else {
                System.out.println("> \"" + bookTitle + "\" not found in any of the libraries.\n");
            }
        }
    }
}
